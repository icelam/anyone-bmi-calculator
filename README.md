# 任何仁BMI計算機 #

### 甚麼是「任何仁」？ ###

任何仁（英语：Anyone，谐音“任何人”）是香港消防處於2018年推出的一個身穿蓝色全包緊身衣的吉祥物及代言人，創造該虛擬角色的用意是為了向公眾推廣「只要敢，就救到人」的信息。其諧音「任何人」，亦寓意「任何人都可以救人」。（資料來源：[維基百科](https://zh.wikipedia.org/wiki/%E4%BB%BB%E4%BD%95%E4%BB%81)）

### Installation ###

* `npm install`

### Dev Server ###

* `npm run dev`

### Build Production ###

All the build files can be found in `/dist` folder.

* `npm run build`
